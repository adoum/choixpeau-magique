from browser import doc, html
import choixpeau as cp


question_nb = 0
reponse_tab = []
question_table = cp.table_csv("Questions.csv")

def affichage():
    doc["Questions"].textContent = question_table[question_nb]['Questions']
    doc["Reponse1"].textContent = question_table[question_nb]['Reponse1']
    doc["Reponse2"].textContent = question_table[question_nb]['Reponse2']
    doc["Reponse3"].textContent = question_table[question_nb]['Reponse3']

def question_clic(ev):
    global question_nb
    reponse_tab.append(question_table[question_nb]['k_reponse' + ev.target.value])
    id_char = cp.id_characteristics(reponse_tab)

    if question_nb < len(question_table) - 1:
        question_nb = question_nb + 1
        affichage()
    else:
        resultats()

def resultats():
    choixpeau_resultats = cp.choixpeau_sort(reponse_tab)
    doc["commence"].style.display = "none"
    doc["hr1"].style.display = "none"
    doc["hr2"].style.display = "none"
    doc["quest1"].style.display = "none"
    doc["quest2"].style.display = "none"
    doc["footer-basic"].style.display = "none"

    doc["Questions"].style.display = "none"
    doc["Reponse1"].style.display = "none"
    doc["Reponse2"].style.display = "none"
    doc["Reponse3"].style.display = "none"
    doc <= html.P("Bravo, tu appartiens à cette maison !", id="quest2")
    doc <= html.IMG(src=choixpeau_resultats[1][0].lower() + ".png", alt="Maison des " + choixpeau_resultats[1][0])
    doc <= html.P("Tes personnages les plus proches sont :", id="quest2")
    doc <= html.H3(choixpeau_resultats[0][0][0] + " qui appartient à la maison des " + choixpeau_resultats[0][0][1] + " !")
    doc <= html.H3(choixpeau_resultats[0][1][0] + " qui appartient à la maison des " + choixpeau_resultats[0][1][1] + " !")
    doc <= html.H3(choixpeau_resultats[0][2][0] + " qui appartient à la maison des " + choixpeau_resultats[0][2][1] + " !")
    doc <= html.H3(choixpeau_resultats[0][3][0] + " qui appartient à la maison des " + choixpeau_resultats[0][3][1] + " !")
    doc <= html.H3(choixpeau_resultats[0][4][0] + " qui appartient à la maison des " + choixpeau_resultats[0][4][1] + " !")
    doc <= html.BUTTON("Recommencer", id="F5")
    doc["F5"].classList.add("button")
    doc["F5"].bind("click", recommencer)
    
def recommencer(ev):
    doc["F5"] <= html.META(http_equiv="Refresh", content="0.1")

affichage()
doc["Reponse1"].bind("click", question_clic)
doc["Reponse2"].bind("click", question_clic)
doc["Reponse3"].bind("click", question_clic)

