# coding: utf-8

'''
Projet Choipeaux magique (1ère partie) :
Auteurs : SAADANA Adam, LAANAOUI Ayoub, BIGEY--ROUX Maxime
Description du programme : Fournir un profil d'éléve cible
Respect PEP-8 approuvé !

Précisions :

Distance = distance euclidienne avec :
sqrt( (C2-C1)**2 + (A2-A1)**2 + (I2-I1)**2 + (G2-G1)**2 )

k = 5 (changeable dans l'IHM)
'''

from math import sqrt
import csv

# Import csv
with open("Characters.csv", mode="r", encoding="utf-8") as f:
    edit = csv.DictReader(f, delimiter=';')
    characters_tab = [{key: value.replace('\xa0', ' ') for key, value
    in element.items()} for element in edit]

with open("Caracteristiques_des_persos.csv", mode="r", encoding="utf-8") as f:
    edit = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key: value.replace('\xa0', ' ') for key, value
    in element.items()} for element in edit]

# Fusion de tables de characteristics_tab et characters_tab
poudlard_characters = []
for element in characteristics_tab:
    for i in characters_tab:
        if element["Name"] == i["Name"]:
            poudlard_characters.append(element)
            poudlard_characters[-1].update(i)
            del poudlard_characters[-1]["Id"]

# Conversion des caractéristiques en entier
for element in characteristics_tab:
    element['Courage'] = int(element['Courage'])
    element['Ambition'] = int(element['Ambition'])
    element['Intelligence'] = int(element['Intelligence'])
    element['Good'] = int(element['Good'])
    for perso in characters_tab:
        if element['Name'] == perso['Name']:
            element['House'] = perso['House']


# Personnages du cahier des charges
personnage_1 = {'Courage': 9, 'Ambition': 2, 'Intelligence': 8, 'Good': 9}
personnage_2 = {'Courage': 6, 'Ambition': 7, 'Intelligence': 9, 'Good': 7}
personnage_3 = {'Courage': 3, 'Ambition': 8, 'Intelligence': 6, 'Good': 3}
personnage_4 = {'Courage': 2, 'Ambition': 3, 'Intelligence': 7, 'Good': 8}
personnage_5 = {'Courage': 3, 'Ambition': 4, 'Intelligence': 8, 'Good': 8}


def distance(perso1, perso2):
    '''
    Fonction qui calcule la distance entre deux personnages
    Entrée : deux dictionnaires(caractéristiques des perso)
    Sortie : flottant qui représente la distance
    '''
    return sqrt((perso1['Good'] - perso2['Good'])**2 + (perso1['Courage'] -
                perso2['Courage'])**2 + (perso1['Ambition'] -
                perso2['Ambition'])**2 + (perso1['Intelligence'] -
                perso2['Intelligence'])**2)


def tab_distance(table, perso):
    '''
    Fonction qui regroupe les données da la fonction distance en table
    Entrée : table et dictionnaire(caractéristiques du perso)
    Sortie : liste regroupé
    '''
    liste_distance = []
    for element in table:
        liste_distance.append([element, distance(element, perso)])
    return liste_distance


def house(liste_kPPV):
    '''
    Fonction qui détermine la maison du personnage
    Entrée : dictionnaire avec les maisons en anglais
    Sortie : chaine de charactères trié avec méthode .sort
    '''
    houses = {"Gryffindor": 0, "Slytherin": 0, "Ravenclaw": 0, "Hufflepuff": 0}
    for element in liste_kPPV:
        houses[element[1]] += 1
    house_k = list(houses.items())
    house_k.sort(key=lambda x: x[1], reverse=True)
    return house_k[0][0]


def kPPV(k, liste):
    '''
    Fonction qui détermine les kPPV
    Entrée : liste et k(entier)
    Sortie : table contenant les kPPV
    '''
    kNN = []
    liste.sort(key=lambda x: x[1])
    for i in range(k):
        kNN.append([liste[i][0]['Name'], liste[i][0]['House']])
    return kNN


def affichage(perso, table, k):
    '''
    Fonction qui sert à l'IHM
    Entrée : k(entier), dictionnaire(caractéristiques du perso)
    et table(celle de tous les personnages)
    Sortie : Affichage et début IHM...
    '''
    voisins = kPPV(k, tab_distance(table, perso))
    house_k = house(voisins)
    return print(f"Ce personnage a comme voisins : \n{voisins}\n"
               f"Ce personnage ira donc dans la maison suivante : {house_k}\n")

# IHM

# Affichage des personnages du cahier des charges
print('Premier profil du personnage : \n ')
affichage(personnage_1, poudlard_characters, 5)
print('Deuxième profil du personnage : \n ')
affichage(personnage_2, poudlard_characters, 5)
print('Troisième profil du personnage : \n ')
affichage(personnage_3, poudlard_characters, 5)
print('Quatrième profil du personnage : \n ')
affichage(personnage_4, poudlard_characters, 5)
print('Cinquième profil du personnage : \n ')
affichage(personnage_5, poudlard_characters, 5)

# Bonus : Choix de la valeur de k possible
if input("Voulez vous choisir une valeur de k ?"
     "\nSinon celle-ci sera égale à 5 !\nOui ou Non : ") == "Oui" or "OUI":
    k = int(input("Entrer votre valeur de k : "))
else:
    k = 5

# Bonus : Saisie manuelle d'un personnage
print("\nIndiquez les caractéristiques de votre personnage : \n ")
C = int(input('Courage : '))
A = int(input("Ambition : "))
I = int(input("Intelligence : "))
G = int(input("Bonté : "))
print()
caracteristiques = {'Courage': C, 'Ambition': A, 'Intelligence': I, 'Good': G}

affichage(caracteristiques, poudlard_characters, k)
