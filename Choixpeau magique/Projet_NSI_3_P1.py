# -*- coding: utf-8 -*-
'''
Projet Choipeaux magique (1ère partie) :
Auteurs : SAADANA Adam, LAANAOUI Ayoub, BIGEY--ROUX Maxime
Description du programme : Fournir un profil d'éléve cible
'''
import csv
from math import sqrt

keys = ['Name', 'Courage', 'Ambition', 'Intelligence', 'Good', 'Gender', 'Job', 'House', 'Wand', 'Patronus', 'Species', 'Blood status', 'Hair colour', 'Eye colour', 'Loyalty', 'Skills', 'Birth', 'Death']

with open("Characters.csv", mode = "r", encoding = "utf-8") as f:
    edit = csv.DictReader(f, delimiter=';')
    characters_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in edit]

with open("Caracteristiques_des_persos.csv", mode = "r", encoding = "utf-8") as f:
    edit = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in edit]
    
poudlard_characters = []

for i in characters_tab:
    for j in characteristics_tab:
        if i["Name"] == j["Name"]:
            i.update(j)
            poudlard_characters.append(i)
print(poudlard_characters)

for element in poudlard_characters:
    element['Courage'] = int(element['Courage'])
    element['Ambition'] = int(element['Ambition'])
    element['Intelligence'] = int(element['Intelligence'])
    element['Good'] = int(element['Good'])
'''
• Courage : 9 Ambition : 2 Intelligence : 8 Good : 9
• Courage : 6 Ambition : 7 Intelligence : 9 Good : 7
• Courage : 3 Ambition : 8 Intelligence : 6 Good : 3
• Courage : 2 Ambition : 3 Intelligence : 7 Good : 8
• Courage : 3 Ambition : 4 Intelligence : 8 Good : 8

Distance = distance euclidienne avec : math.sqrt( (C2-C1)**2 + (A2-A1)**2 + (I2-I1)**2 + (G2-G1)**2 )
k = 5 (changeable dans l'IHM)
'''

def distance(joueur1, joueur2):
    return sqrt((joueur1['Courage'] - joueur2['Courage']) ** 2
                + (joueur1['Ambition'] - joueur2['Ambition']) ** 2
                + (joueur1['Intelligence'] - joueur2['Intelligence']) ** 2
                + (joueur1['Good'] - joueur2['Good']) ** 2)

profils_a_tester = [
    {"Courage":9, "Ambition":2, "Intelligence":8, "Good":9},
    {"Courage":6, "Ambition":7, "Intelligence":9, "Good":7},
    {"Courage":3, "Ambition":8, "Intelligence":6, "Good":3},
    {"Courage":2, "Ambition":3, "Intelligence":7, "Good":8},
    {"Courage":3, "Ambition":4, "Intelligence":8, "Good":8}
]

k = 5
for l in profils_a_tester:
    poudlard_characters.sort(key=lambda x: distance(l, x))
    print(poudlard_characters[:k])
    
for 
